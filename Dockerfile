FROM ubuntu:latest
MAINTAINER Benedikt Lang <mail@blang.io>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q && apt-get install -qy \
    texlive-full \
    python-pygments gnuplot \
    make git inkscape \
    graphviz ttf-dejavu \
    pandoc \
    && \
    rm -rf /var/lib/apt/lists/* \
    && \
    luaotfload-tool --update

#  ttf-freefont ttf-liberation ttf-linux-libertine \
    
WORKDIR /data
VOLUME ["/data"]
